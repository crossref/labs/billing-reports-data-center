#!/bin/bash

set -e

activate_venv() {
    if [ -f "./venv/bin/activate" ]; then
        echo "Activating virtual environment..."
        source ./venv/bin/activate
    else
        echo "Error: Virtual environment not found. Please create a virtual environment first."
        exit 1
    fi
}

# Default directories and parameters
metadata_dir="./data/member_data"
quarterly_dir="./data/quarterlies"
quarter=""
year=""

print_help() {
    echo "Usage: $0 [OPTIONS]"
    echo ""
    echo "Options:"
    echo "  --metadata-dir DIR      Directory to store member metadata (default: ./data/member_data)"
    echo "  --quarterly-dir DIR     Directory to store quarterly files (default: ./data/quarterlies)"
    echo "  --quarter Q             Quarter to process (one of: Q1, Q2, Q3, Q4)"
    echo "  --year YYYY             Year to process (e.g., 2023)"
    echo "  --help                  Display this help message"
    echo ""
    echo "Environment Variables:"
    echo "  CR_ADMIN_USER           Crossref admin username (required)"
    echo "  CR_ADMIN_ROLE           Crossref admin role (required)"
    echo "  CR_ADMIN_PWD            Crossref admin password (required)"
    exit 0
}


validate_quarter() {
    case "$1" in
        Q1|Q2|Q3|Q4) ;;
        *) echo "Error: Invalid quarter '$1'. Must be one of Q1, Q2, Q3, Q4."; exit 1 ;;
    esac
}

calculate_year() {
    if [[ "$quarter" == "Q4" ]]; then
        # Check if we are on macOS or Linux and set the year accordingly
        if [[ "$(uname)" == "Darwin" ]]; then
            year=$(date -v-1y +%Y)  # macOS date command
        else
            year=$(date --date="last year" +%Y)  # Linux date command
        fi
    else
        year=$(date +%Y)  # Current year for Q1, Q2, Q3
    fi
}

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        --metadata-dir) metadata_dir="$2"; shift ;;
        --quarterly-dir) quarterly_dir="$2"; shift ;;
        --quarter) quarter="$2"; validate_quarter "$quarter"; shift ;;
        --year) year="$2"; shift ;;
        --help) print_help ;;
        *) echo "Unknown parameter: $1"; exit 1 ;;
    esac
    shift
done

if [[ -z "$year" ]]; then
    calculate_year
fi

echo "Using metadata directory: $metadata_dir"
echo "Using quarterly directory: $quarterly_dir"

if [[ -n "$quarter" ]]; then
    echo "Processing for quarter: $quarter"
fi

if [[ -n "$year" ]]; then
    echo "Processing for year: $year"
fi

if [[ -z "$VIRTUAL_ENV" ]]; then
    activate_venv
else
    echo "Virtual environment is already active."
fi

mkdir -p "$metadata_dir"

python3 cli.py download-member-metadata --results-dir "$metadata_dir"

mkdir -p "$quarterly_dir"

billing_args=("--skip-s3-upload" "--metadata-dir" "$metadata_dir" "--quarterly-dir" "$quarterly_dir")

if [[ -n "$quarter" ]]; then
    billing_args+=("--quarter" "$quarter")
fi

if [[ -n "$year" ]]; then
    billing_args+=("--year" "$year")
fi

python cli.py generate-billing-files "${billing_args[@]}"

python cli.py populate-labs-api --quarterlies-dir "$quarterly_dir"
