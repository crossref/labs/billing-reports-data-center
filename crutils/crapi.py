import os
import logging
from datetime import timedelta

import requests
from diskcache import Cache

from crutils.common import USER_AGENT, APIConnectionException

CACHE_TIMEOUT = 60 * 60 * 24


CRAPI_URL = "https://api.crossref.org"

QS_URL = os.getenv("QS_URL", "http://bstn-prod-qs-03.crossref.org:8085")
QS_PROXY = dict(http="socks5h://localhost:8123")


cache = Cache("_billing_reports_cache")

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)


def member_prefixes(member):
    yield from member["prefixes"]


def member_data(member_id):
    try:
        res = requests.get(
            f"{CRAPI_URL}/members/{member_id}",
            headers={"User-Agent": USER_AGENT},
        )
    except Exception as e:
        raise APIConnectionException(e) from e
    return res.json()["message"] if res.status_code == 200 else None


# This will be cached because any particular depositor code may
# deposit on behalf of many members, and we don't want to hit the
# DB multiple times as we generate results for each member.
@cache.memoize(expire=CACHE_TIMEOUT, typed=True)
def get_billing_data(depositor_code, period, qs_proxy):
    try:
        proxy = dict(http=f"{qs_proxy}") if qs_proxy else None
        url = f"{QS_URL}/depreport?mode=titlesByQuarter&user={depositor_code}&month={period}&format=csv"
        logger.info(f"Getting billing data from {url}")
        # res = requests.get(url, headers={"User-Agent": USER_AGENT}, proxies=QS_PROXY)
        res = requests.get(
            url, headers={"User-Agent": USER_AGENT}, proxies=proxy
        )

    except Exception as e:
        raise APIConnectionException(e) from e

    if res.status_code != 200:
        logger.error(f"Error getting billing data from {url}")
        return None

    # return res.text if res.status_code == 200 else None
    return res.text
