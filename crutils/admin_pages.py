import os
import sys
import logging
import requests
import json
from bs4 import BeautifulSoup as bs
from lxml import etree
from urllib.parse import quote
from slugify import slugify

from crutils.common import APIConnectionException, USER_AGENT

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


PUB_ADMIN_PAGE_URL = "https://doi.crossref.org/servlet/publisherUserAdmin"

ADMIN_USER_TABLE_NUM = 5
ADMIN_TABLE_NUM = 5


def credentials():
    pwd = quote(os.environ.get("CR_ADMIN_PWD", default="xxx"))
    usr = quote(os.environ.get("CR_ADMIN_USR", default="yyy"))
    role = quote(os.environ.get("CR_ADMIN_ROLE", default="publisher"))
    return f"usr={usr}&pwd={pwd}&username={usr}&role={role}&role={role}&password={pwd}"


def search_credentials(prefix):
    pwd = quote(os.environ.get("CR_ADMIN_PWD", default="xxx"))
    usr = quote(os.environ.get("CR_ADMIN_USR", default="yyy"))
    role = quote(os.environ.get("CR_ADMIN_ROLE", default="publisher"))
    return f"usr={usr}&pwd={pwd}&username={usr}&role={role}&role={role}&password={pwd}&doi={prefix}&sf=search"


def admin_system_headers():
    return {
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": USER_AGENT,
    }


def search_admin_page(url, prefix):
    # logger.info(f"URL: {url}")
    try:
        res = requests.request(
            "POST",
            url,
            headers=admin_system_headers(),
            data=search_credentials(prefix=prefix),
        )
    except Exception as e:
        raise APIConnectionException(e) from e
    return res.text if res.status_code == 200 else None


def chunks(l: list(), n: int) -> list():
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i : i + n]


def fill_empty_keys(keys):
    return [k or f"empty_{i}" for i, k in enumerate(keys)]


def admin_page(url):
    logger.info(f"URL: {url}")
    try:
        res = requests.request(
            "POST", url, headers=admin_system_headers(), data=credentials()
        )
    except Exception as e:
        raise APIConnectionException(e) from e
    return res.text if res.status_code == 200 else None


def table_header(table):
    return fill_empty_keys([slugify(h.text) for h in table("th")])


def cell_content(cell):
    return (
        cell.text.strip()
        if cell.text
        else (cell.find("input").get("value") if cell.find("input") else "")
    )


def table_data(table, row_size):
    return list(
        chunks(
            [cell_content(cell) for row in table("tr") for cell in row("td")],
            row_size,
        )
    )


def nth_table(table_num, html):
    soup = bs(html, features="html.parser")
    return soup.findAll("table")[table_num]


## Parse and make sense of publisher admin user pages


def member_admin_user_table(html):
    soup = bs(html, features="html.parser")
    return soup.findAll("table")[ADMIN_USER_TABLE_NUM]


def member_admin_user_page(publisher_id):
    return admin_page(
        f"{PUB_ADMIN_PAGE_URL}?sf=viewMembers&publisherID={publisher_id}"
    )


def member_admin_user_info(html):
    table = member_admin_user_table(html)
    header = table_header(table)
    data = table_data(table, len(header))
    return [dict(zip(header, chunk)) for chunk in data]


## Parse and make sense of publisher admin pages


def member_table_data(table):
    return [cell.text.strip() for row in table("tr") for cell in row("td")]


def member_admin_page(prefix):
    return admin_page(
        f"{PUB_ADMIN_PAGE_URL}?sf=search&doi={prefix}&start=0&size=20"
    )


def member_admin_table(html):
    return nth_table(ADMIN_TABLE_NUM, html)


def member_admin_info(html):
    table = member_admin_table(html)
    header = table_header(table)
    data = table_data(table, len(header))
    return [dict(zip(header, chunk)) for chunk in data][0]


def xmember_admin_info(html):
    table = member_admin_table(html)
    header = table_header(table)
    data = table_data(table, len(header))
    return [dict(zip(header, chunk)) for chunk in data]


def strip_status(pub_id):
    # sometimes the table cell contains status information or other info
    # in addtion to the publisher_id
    # strip publisher_id of non-numeric characters (e.g., (DELETED))
    return "".join([c for c in pub_id if c.isdigit()])


def publisher_id(admin_dict):
    return strip_status(admin_dict["publisher-id"])


def customer_id(admin_dict):
    return admin_dict["customer-id"]


def description(admin_dict):
    return admin_dict["desc"]


def xpublisher_id(admin_list):
    if len(admin_list) == 1:
        return admin_list[0]["publisher-id"]
    logger.warning(f"Multiple publisher IDs found: {admin_list}")
    return None
