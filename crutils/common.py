import logging
import random
from pathlib import Path

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# USER_AGENT = 'Crossref Deposit Users; mailto:labs@crossref.org'
USER_AGENT = "crossref_billy_bob"


class APIConnectionException(Exception):
    pass


def get_member_metadata_dir(results_dir, member_id):
    member_metadata_dir = results_dir / str(member_id)
    Path(member_metadata_dir).mkdir(parents=True, exist_ok=True)
    return member_metadata_dir


def get_member_metadata_path(results_dir, member_id):
    return get_member_metadata_dir(results_dir, member_id) / "metadata.json"


## Dealing with member records


def single_member_record(member_id, members):
    logger.info("Single mode")
    return [member for member in members if member["id"] == member_id]


def random_subset(l, max):
    logger.info("Random subset")
    random.shuffle(l)
    return l[:max]


def records_with_dois(members):
    logger.info("Records with DOIs")
    return [
        member
        for member in members
        if "counts" in member and member["counts"]["total-dois"] > 0
    ]


def max_subset(l, max):
    logger.info(f"Bulk mode. Max set to {max}")
    return random_subset(l, max) if max else l


def user_selection(members, member_id, max):
    return (
        single_member_record(member_id, members)
        if member_id
        else records_with_dois(max_subset(members, max))
    )


## Dealing with billing metadata records


def get_member_billing_dir(results_dir, member_id, period_name):
    billing_dir = results_dir / f"{member_id}/{period_name}"
    Path(billing_dir).mkdir(parents=True, exist_ok=True)
    return billing_dir


def get_member_billing_path(results_dir, member_id, period_name, name):
    return get_member_billing_dir(results_dir, member_id, period_name) / name


def single_billing_metadata(member_id, billing_metadata):
    logger.info(f"Single mode: {member_id}")
    return [record for record in billing_metadata if record["id"] == member_id]
