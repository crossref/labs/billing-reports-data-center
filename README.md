# Billing Reports Generator
Billing Reports Generator is a system that generates quarterly billing summaries and deposits them in S3.

![license](https://img.shields.io/gitlab/license/crossref/labs/billing-reports-data-center) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/billing-reports-data-center)

![AWS](https://img.shields.io/badge/AWS-%23FF9900.svg?style=for-the-badge&logo=amazon-aws&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

## Requirements

* Internal access to Crossref data center either via VPN or running natively

## Environment variables that must be set
    AWS_ACCESS_KEY_ID = the AWS access key
    AWS_SECRET_ACCESS_KEY = the AWS secret key
    CR_ADMIN_USR = the username for the Crossref admin account
    CR_ADMIN_PWD = the password for the Crossref admin account
    CR_ADMIN_ROLE = the role for the Crossref admin account

## Usage

     Usage: cli.py [OPTIONS] COMMAND [ARGS]...                                                                                                                                                                                              
                                                                                                                                                                                                                                            
    ╭─ Options ──────────────────────────────────────────────────────────────────────────────────────────────────────╮
    │ --install-completion          Install completion for the current shell.                                        │
    │ --show-completion             Show completion for the current shell, to copy it or customize the installation. │
    │ --help                        Show this message and exit.                                                      │
    ╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯
    ╭─ Commands ─────────────────────────────────────────────────────────────────────────────────────────────────────╮
    │ clone-member-metadata      Clone existing member metadata from another instance                                │
    │ download-member-metadata   Download the member metadata into a dictionary and dump to console                  │
    │ generate-billing-files     Create local and S3 billing files                                                   │
    │ populate-labs-api          Load the data into the Labs API                                                     │
    │ populate-publisher-ids     Populate the publisher ids for a member                                             │
    │ test-log                   Test logging to Slack                                                               │
    │ test-member-file           Download the member file from CI and dump to console                                │
    ╰────────────────────────────────────────────────────────────────────────────────────────────────────────────────╯

# Key Commands

## `download-member-metadata`
Downloads initial member metadata from the Crossref admin system and stores it in a dictionary as well as on-disk. This is the first stage in generating a quarterly download.

## `generate-billing-files`
Iterates over every member, scraping the specified quarterly billing data and then uploads it to S3. This is the second stage in generating a quarterly download.

## `populate-labs-api`
Writes the quarterly billing data into the Labs API annotation system.

## `populate-publisher-ids`
Scrapes the admin console to find the internal "publisher id" for each member and then pushes these as Labs API annotations.

&copy; Crossref 2024