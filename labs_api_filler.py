import os
import json
from smart_open import open


class LabsAPIFiller:
    def __init__(self, quarterlies_directory, logger, aws_connector, bucket):
        self.quarterlies_directory = quarterlies_directory
        self.logger = logger
        self.aws_connector = aws_connector
        self.bucket = bucket

    def fill(self, member_id=None):
        """
        Populate the Labs API for all members or a specific member.

        Parameters:
            member_id (str): Optional. A specific member ID to process. If None, process all members.
        """
        if member_id:
            members = [os.path.join(self.quarterlies_directory, member_id)]
            if not os.path.isdir(members[0]):
                self.logger.error(f"Member directory not found for ID: {member_id}")
                return
        else:
            members = [
                f.path for f in os.scandir(self.quarterlies_directory) if f.is_dir()
            ]

        for member in members:
            quarterlies = [f.path for f in os.scandir(member) if f.is_dir()]
            member_id = member.split("/")[-1]

            # Fetch existing quarterlies from S3
            s3_id = f"s3://{self.bucket}/annotations/members/{member_id}/quarterlies.json"
            try:
                with open(s3_id, "r", transport_params={"client": self.aws_connector.s3_client}) as input_file:
                    existing_data = json.load(input_file)
                    existing_quarterlies = set(existing_data.get("quarterlies", []))
                self.logger.info(f"Existing S3 quarterlies for member {member_id}: {sorted(existing_quarterlies)}")
            except Exception as e:
                self.logger.warning(f"Could not fetch existing quarterlies for member {member_id}: {e}")
                existing_quarterlies = set()

            # Get local quarterlies
            local_quarterlies = {quart.split("/")[-1] for quart in quarterlies}
            self.logger.info(f"Local quarterlies for member {member_id}: {sorted(local_quarterlies)}")

            # Merge quarters
            merged_quarterlies = sorted(existing_quarterlies.union(local_quarterlies))
            self.logger.info(f"Merged quarterlies for member {member_id}: {merged_quarterlies}")

            json_line = {
                "quarterlies": merged_quarterlies
            }

            # Write back to S3
            try:
                with open(
                    s3_id,
                    "w",
                    transport_params={"client": self.aws_connector.s3_client},
                ) as output:
                    output.write(json.dumps(json_line))
                    output.flush()

                self.logger.info(f"Updated quarterlies written to {s3_id} for member {member_id}")
            except Exception as e:
                self.logger.error(f"Failed to write data for member {member_id} to S3: {e}")

    def fast_scandir(self, dirname):
        subfolders = [f.path for f in os.scandir(dirname) if f.is_dir()]
        for dirname in list(subfolders):
            subfolders.extend(self.fast_scandir(dirname))
        return subfolders
