from smart_open import open


class S3Exporter:
    def __init__(self, aws_connector, bucket):
        self.aws_connector = aws_connector
        self.bucket = bucket

    def export(self, path, data):
        url = f"s3://{self.bucket}/{path}"

        with open(
            url,
            "wb",
            transport_params={"client": self.aws_connector.s3_client},
        ) as output:
            print("Data length is: " + str(len(data)))
            output.write(data)
            output.flush()
