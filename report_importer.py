import datetime
import json
import logging
import os
from enum import Enum
from io import StringIO
from pathlib import Path

from claws.aws_utils import AWSConnector
import pandas as pd

import export_to_s3
from crutils.common import (
    max_subset,
    single_billing_metadata,
    get_member_billing_dir,
)
from crutils.crapi import get_billing_data

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class Quarters(str, Enum):
    Q1 = "Q1"
    Q2 = "Q2"
    Q3 = "Q3"
    Q4 = "Q4"


class ReportImporter:
    COLUMN_ORDER = [
        "Type",
        "Title",
        "Prefix",
        "publisher-id",
        "customer-id",
        "description",
        "user-name",
        "user-id",
        "# CY",
        "# BY",
    ]

    def __init__(self, member_id, quarter, year, logger):
        self.member_id = member_id
        self.quarter = quarter
        self.year = year
        self.logger = logger

    @staticmethod
    def _current_quarter() -> str:
        return f"Q{pd.Timestamp(datetime.datetime.now()).quarter}"

    def _period_to_iso_date(self, period_name):
        period = pd.Period(period_name, freq="Q")
        return f"{period.year}-{period.month}-{period.day}"

    def _get_all_metadata_files(self, results_dir):
        return list(results_dir.rglob("metadata.json"))

    def _read_metadata_file(self, path):
        with open(path, "r") as metadata_file:
            return json.load(metadata_file)

    def _remove_total(self, csv_data):
        # remove last line of csv_data- it contains the total
        return "\n".join(csv_data.splitlines()[:-1])

    def _billing_data_to_df(
        self,
        billing_data,
        user_name="XXX",
        user_id="YYY",
        customer_id="ZZZ",
        publisher_id="AAA",
        description="BBB",
    ):
        return pd.read_csv(
            StringIO(self._remove_total(billing_data)),
            sep=",",
            header=0,
            index_col=False,
            dtype=str,
        )

    def _add_context_to_billing_data_df(
        self,
        df,
        customer_id,
        publisher_id,
        description,
        user_name="XXX",
        user_id="YYY",
    ):
        df["publisher-id"] = publisher_id
        df["customer-id"] = customer_id
        df["description"] = description
        df["user-name"] = user_name
        df["user-id"] = user_id
        logger.info(f">>>Added {user_name} {user_id} to billing data context")
        return df

    def _records_with_this_prefix(self, df, prefix):
        return df[df["Prefix"] == prefix]

    def _has_activity(self, df):
        try:
            return df[(df["# CY"] != "0") | (df["# BY"] != "0")]
        except KeyError as e:
            logger.error(f"Invalid record: {df}")
            logger.error(f"KeyError: {e}")
            return False

    def _filter_billing_metadata_by_user_prefs(
        self, billing_metadata, member_id, max
    ):
        return (
            single_billing_metadata(member_id, billing_metadata)
            if member_id
            else max_subset(billing_metadata, max)
        )

    def _read_all_billing_metadata(self, results_dir):
        return [
            self._read_metadata_file(path)
            for path in self._get_all_metadata_files(results_dir)
        ]

    def import_report(
        self,
        input_dir: Path,
        output_dir: Path,
        skip_s3_upload: bool = False,
    ):
        max: int = 0
        bucket = os.environ.get("BUCKET_NAME", "sandbox.research.crossref.org")

        aws_connector = AWSConnector(
            bucket=bucket,
            aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
            aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"],
            unsigned=False,
        )

        # Import the report
        period_name = f"{self.year}{self.quarter}"
        print(f"period_name: {period_name}")
        period_date = self._period_to_iso_date(period_name)

        logger.info(
            f"Generating billing files for quarter: {self.quarter} {self.year} ({period_date})"
        )

        billing_metadata = self._read_all_billing_metadata(input_dir)
        billing_metadata = self._filter_billing_metadata_by_user_prefs(
            billing_metadata, self.member_id, max
        )

        for record in billing_metadata:
            accumulated_billing_data_dfs = []
            quarterly_path = get_member_billing_dir(
                output_dir, record["id"], period_name
            )

            quarterly_path_s3 = f"quarterlies/{record['id']}/{period_name}"
            print(quarterly_path_s3)

            logger.info(f"member: {record['id']} - {record['name']}")

            for prefix, prefix_data in record["prefixes"].items():
                publisher_id = prefix_data["publisher-id"]
                customer_id = prefix_data["customer-id"]
                description = prefix_data["description"]

                logger.info(f"\tprefix: {prefix} - {description} ")

                for user in prefix_data["users"]:
                    logger.info(
                        f"\t\tdepositor: {user['user-id']} {user['name']}"
                    )

                    if (
                        user["name"] == ""
                    ):  # for example, see users under Informa UK (301) prefix: 10.1623
                        logger.warning(
                            f"\t\t\tNo name for user {user['user-id']}"
                        )
                        continue

                    # Use CS to get billing data for this code and period, save to file
                    # even if it has no data relevant for this member as this
                    # mimics what support does now and so files can be can be used
                    # by support to spot-check that this script is working
                    # correctly

                    billing_data = get_billing_data(
                        user["name"], period_date, None
                    )

                    # handle https://crossref.atlassian.net/browse/CR-1920
                    if not billing_data:
                        continue

                    billing_path = (
                        quarterly_path / f"{user['name']}-{user['user-id']}.csv"
                    )
                    logger.info(
                        f"\t\tbilling file download path: {billing_path}"
                    )
                    with open(billing_path, "w") as billing_file:
                        billing_file.write(billing_data)

                    # Now we can convert the billing data to a dataframe and filter
                    # out rows that are not relevant to this member or that have no
                    # data. This is currently done manually by support.

                    # convert billing data to dataframe
                    billing_data_df = self._billing_data_to_df(billing_data)

                    # only keep rows that have activity
                    billing_data_df = self._has_activity(billing_data_df)

                    # only keep rows that have this prefix
                    billing_data_df = self._records_with_this_prefix(
                        billing_data_df, prefix
                    )

                    if len(billing_data_df) == 0:
                        logger.warning(
                            f"\t\tno data for depositor: {user['name']} deposting under prefix: {prefix} for member: {record['id']}"
                        )
                        continue
                    logger.info(
                        f"\t\tfound {len(billing_data_df)} records for depositor: {user['name']} deposting under prefix: {prefix} for member: {record['id']}"
                    )
                    # add context to the billing data
                    # , customer_id, publisher_id, description
                    billing_data_df = self._add_context_to_billing_data_df(
                        billing_data_df,
                        customer_id,
                        publisher_id,
                        description,
                        user["name"],
                        user["user-id"],
                    )
                    accumulated_billing_data_dfs.append(billing_data_df)

            # now we have all the billing data for this member, we can combine it
            # into a single dataframe and save it to file
            if len(accumulated_billing_data_dfs) == 0:
                logger.warning(f"\t\tno data for member: {record['id']}")
                continue

            accumulated_billing_data_df = pd.concat(
                accumulated_billing_data_dfs, ignore_index=True
            )
            accumulated_billing_data_df = accumulated_billing_data_df[
                self.COLUMN_ORDER
            ]
            accumulated_billing_data_df = accumulated_billing_data_df.astype(
                {"# CY": "int", "# BY": "int"}
            )
            accumulated_billing_data_df.to_excel(
                quarterly_path / "all.xls", index=True
            )
            accumulated_billing_data_df.to_parquet(
                quarterly_path / "all.parquet", index=True
            )

            # Upload to S3 only if skip_s3_upload is False
            if not skip_s3_upload:
                s3_path = f"{quarterly_path_s3}/all.parquet"
                exporter = export_to_s3.S3Exporter(
                    aws_connector=aws_connector, bucket=bucket
                )
                exporter.export(
                    s3_path,
                    accumulated_billing_data_df.to_parquet(path=None, index=True),
                )   