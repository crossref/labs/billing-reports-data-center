from slack_logger import SlackLogger
from secret import Secret
import logging

class SlackLog:
    def __init__(self):
        self.token = Secret.get_secret("slack_labs_infrastructure")[
            "slack_bot_token"
        ]
        self.options = {
            "service_name": "Billing report generator",
            "service_environment": "Production",
            "display_hostname": True,
            "default_level": "info",
        }

        self.logger = SlackLogger(token=self.token, **self.options)
        self.channel = "#labs-infrastructure"

    def send(self, title, message):
        """Send an INFO message to Slack"""
        response = self.logger.send(
            channel=self.channel,
            title=title,
            description=message,
        )
        return response

import logging

class ConsoleLog:
    def __init__(self):
        # Configure the logger to output to stdout
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger('ConsoleLogger')

    def send(self, title, message):
        """Log an INFO message with a title and message."""
        full_message = f"{title}: {message}"
        self.logger.info(full_message)

    def __getattr__(self, name):
        """
        Delegate attribute access to the underlying logger.
        This allows direct calls to info, error, etc.
        """
        return getattr(self.logger, name)
