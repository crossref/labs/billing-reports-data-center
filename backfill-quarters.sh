#!/bin/bash

# Configuration
START_YEAR=2023
START_QUARTER=2  # Use 1 for Q1, 2 for Q2, etc.
END_YEAR=2024
END_QUARTER=3
LOG_FILE="/usr/local/qs/billingreport/watchdog_output.log"
BASE_COMMAND="python watchdog.py python3 cli.py generate-billing-files \
    --metadata-dir /usr/local/qs/billingreport/data/member_data \
    --quarterly-dir /usr/local/qs/billingreport/data/quarterlies \
    --skip-s3-upload"

# Function to determine the next quarter and year
next_quarter() {
    local quarter=$1
    local year=$2
    local next_quarter=$((quarter + 1))

    if [[ $next_quarter -gt 4 ]]; then
        next_quarter=1
        year=$((year + 1))
    fi

    echo "$next_quarter $year"
}

# Main loop
current_year=$START_YEAR
current_quarter=$START_QUARTER

source venv/bin/activate

while [[ $current_year -lt $END_YEAR || ($current_year -eq $END_YEAR && $current_quarter -le $END_QUARTER) ]]; do
    # Convert quarter number to Q1, Q2, etc.
    quarter_label="Q$current_quarter"

    echo "Processing $quarter_label $current_year..."
    nohup $BASE_COMMAND --quarter $quarter_label --year $current_year --log-file $LOG_FILE > /dev/null 2>&1 &

    # Wait for the watchdog to finish before starting the next one
    wait

    # Get the next quarter and year
    read current_quarter current_year < <(next_quarter $current_quarter $current_year)
done

echo "All processing complete."
