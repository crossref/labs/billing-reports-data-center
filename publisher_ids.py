import json
import os

from claws.aws_utils import AWSConnector

import crutils.admin_pages
import crutils.crapi

from smart_open import open


class PublisherID:
    def __init__(self, slack_logger, quarterlies_directory):
        self.slack_logger = slack_logger
        self.quarterlies_directory = quarterlies_directory

    def iterate_members(self):
        members = [
            f.path.split("/")[1]
            for f in os.scandir(self.quarterlies_directory)
            if f.is_dir()
        ]

        bucket = os.environ.get("BUCKET_NAME", "sandbox.research.crossref.org")
        aws_connector = AWSConnector(
            bucket=bucket,
            aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
            aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"],
            unsigned=False,
        )

        for member in members:
            result = self.get_member_data(member_id=member)

            s3_id = (
                f"s3://{bucket}/annotations/members/{member}/publisher-ids.json"
            )

            with open(
                s3_id,
                "w",
                transport_params={"client": aws_connector.s3_client},
            ) as output:
                output.write(json.dumps(result))
                output.flush()

            print(f"Data written to {s3_id}")

    def get_member_data(self, member_id):
        # get prefixes for this member from the Crossref API
        member_data = crutils.crapi.member_data(member_id)

        results = {}

        for prefix in member_data["prefixes"]:
            admin_page = crutils.admin_pages.search_admin_page(
                url="https://doi.crossref.org/servlet/publisherUserAdmin",
                prefix=prefix,
            )

            table = crutils.admin_pages.nth_table(table_num=5, html=admin_page)
            header = crutils.admin_pages.table_header(table)
            data = crutils.admin_pages.table_data(table, len(header))

            for row in data:
                if row[2] == prefix:
                    results[prefix] = row[1]

        return results
