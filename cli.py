import datetime
import os
import shutil
from pathlib import Path
from typing import Optional
import subprocess

import typer
from claws.aws_utils import AWSConnector

import crutils.admin_pages
import crutils.crapi
import member_metadata_importer
from labs_api_filler import LabsAPIFiller
from logger import ConsoleLog
from publisher_ids import PublisherID

from report_importer import ReportImporter

app = typer.Typer()


@app.command()
def test_log():
    """
    Test logging to Slack
    """
    logger = ConsoleLog()
    logger.send("Test", "This is a test message")


@app.command()
def test_member_file():
    """
    Download the member file from CI and dump to console
    """
    logger = ConsoleLog()
    mmi = member_metadata_importer.MemberMetadataImporter(logger)
    print(mmi._get_member_data_file())


@app.command()
def download_member_metadata(
    results_dir: Optional[Path] = typer.Option(
        "./member_data",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
    ),
):
    """
    Download the member metadata into a dictionary and dump to console
    """
    logger = ConsoleLog()
    mmi = member_metadata_importer.MemberMetadataImporter(logger)
    mmi.import_member_metadata(results_dir=results_dir)


@app.command()
def populate_labs_api(
    quarterlies_dir: Optional[Path] = typer.Option(
        "./quarterlies",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
    ),
    member_id: Optional[int] = typer.Option(
        None, help="ID of the member to populate Labs API for (default: all members)."
    ),
):
    """
    Load the data into the Labs API for a specific member or all members.
    """
    logger = ConsoleLog()
    bucket = os.environ.get("BUCKET_NAME", "sandbox.research.crossref.org")
    aws_connector = AWSConnector(
        bucket=bucket,
        aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
        aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"],
        unsigned=False,
    )

    laf = LabsAPIFiller(
        quarterlies_directory=quarterlies_dir,
        logger=logger,
        aws_connector=aws_connector,
        bucket=bucket,
    )
    laf.fill(member_id=str(member_id) if member_id else None)


@app.command()
def populate_publisher_ids(
    quarterly_dir: Optional[Path] = typer.Option(
        "./quarterlies",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
    ),
    member: int = 0,
):
    """
    Populate the publisher ids for a member
    """
    logger = ConsoleLog()

    publisher_id_processor = PublisherID(
        slack_logger=logger, quarterlies_directory=quarterly_dir
    )
    if member != 0:
        print(publisher_id_processor.get_member_data(member))
    else:
        publisher_id_processor.iterate_members()

@app.command()
def generate_billing_files(
    metadata_dir: Optional[Path] = typer.Option(
        "./member_data",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
        help="Directory containing member metadata",
    ),
    quarterly_dir: Optional[Path] = typer.Option(
        "./quarterlies",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
        help="Directory to save generated quarterly files",
    ),
    member_id: int = typer.Option(
        0, help="ID of the member to generate files for (default: all members)"
    ),
    quarter: str = typer.Option(
        ReportImporter._current_quarter(),
        help="Quarter to process (e.g., Q1, Q2, Q3, Q4). Defaults to the current quarter.",
    ),
    year: int = typer.Option(
        datetime.datetime.now().year,
        help="Year for the quarter (e.g., 2023). Defaults to the current year.",
    ),
    skip_s3_upload: bool = typer.Option(False, help="Skip uploading files to S3."),
):
    """
    Create local and S3 billing files for a specific quarter and year.
    """
    logger = ConsoleLog()
    logger.info(f"Generating billing files for {quarter} {year}")

    # Initialize the ReportImporter
    report_importer = ReportImporter(
        member_id=member_id, quarter=quarter, year=year, logger=logger
    )

    # Generate billing files
    report_importer.import_report(
        input_dir=metadata_dir, output_dir=quarterly_dir, skip_s3_upload=skip_s3_upload
    )
    logger.info(f"Billing files generated for {quarter} {year}")

@app.command()
def backfill_billing_files(
    start_year: int = typer.Option(..., help="Starting year"),
    start_quarter: str = typer.Option(..., help="Starting quarter (e.g., Q1, Q2, Q3, Q4)"),
    end_year: int = typer.Option(..., help="Ending year"),
    end_quarter: str = typer.Option(..., help="Ending quarter (e.g., Q1, Q2, Q3, Q4)"),
    metadata_dir: Path = typer.Option(
        "./member_data",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
        help="Directory containing member metadata",
    ),
    quarterly_dir: Path = typer.Option(
        "./quarterlies",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
        help="Directory to save generated quarterly files",
    ),
    skip_s3_upload: bool = typer.Option(False, help="Skip uploading files to S3."),
    log_file: Optional[Path] = typer.Option(
        None,
        help="Optional log file to capture output from each execution",
    ),
):
    """
    Backfill billing files for multiple quarters and years.
    """
    logger = ConsoleLog()
    quarters = ["Q1", "Q2", "Q3", "Q4"]
    
    # Validate quarters
    if start_quarter not in quarters or end_quarter not in quarters:
        logger.error("Invalid quarter provided. Use one of Q1, Q2, Q3, Q4.")
        raise typer.Exit(1)
    
    start_quarter_idx = quarters.index(start_quarter)
    end_quarter_idx = quarters.index(end_quarter)

    current_year = start_year
    current_quarter_idx = start_quarter_idx

    while current_year < end_year or (current_year == end_year and current_quarter_idx <= end_quarter_idx):
        current_quarter = quarters[current_quarter_idx]
        logger.info(f"Processing {current_quarter} {current_year}...")
        
        command = [
            "python3",
            "cli.py",
            "generate-billing-files",
            "--metadata-dir", str(metadata_dir),
            "--quarterly-dir", str(quarterly_dir),
            "--quarter", current_quarter,
            "--year", str(current_year),
        ]
        
        if skip_s3_upload:
            command.append("--skip-s3-upload")
        
        # Execute the command
        with open(log_file, "a") if log_file else subprocess.DEVNULL as log:
            process = subprocess.Popen(command, stdout=log, stderr=log)
            process.wait()
            
            if process.returncode != 0:
                logger.error(f"Error processing {current_quarter} {current_year}. Exiting.")
                raise typer.Exit(1)
        
        logger.info(f"Completed {current_quarter} {current_year}.")
        
        # Move to the next quarter
        current_quarter_idx += 1
        if current_quarter_idx == len(quarters):
            current_quarter_idx = 0
            current_year += 1
    
    logger.info("All processing complete.")

@app.command()
def clone_member_metadata(
    src_dir: Optional[Path] = typer.Option(
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
    ),
    results_dir: Optional[Path] = typer.Option(
        "./member_data",
        exists=True,
        file_okay=False,
        dir_okay=True,
        readable=True,
    ),
):
    """
    Clone existing member metadata from another instance
    """
    sub_dirs = [f.path for f in os.scandir(src_dir) if f.is_dir()]

    for directory in sub_dirs:
        src = Path(directory) / "metadata.json"
        dst = Path(results_dir / os.path.basename(directory) / "metadata.json")

        os.makedirs(os.path.dirname(dst), exist_ok=True)
        shutil.copy(src, dst)


if __name__ == "__main__":
    app()
