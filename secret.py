import json

import claws.aws_utils


class Secret:
    def __init__(self):
        pass

    @staticmethod
    def get_secret(secret_name):
        """
        Get a secret from AWS
        :param secret_name: the secret name
        :return: the secret
        """
        bucket = os.environ.get("BUCKET_NAME", "sandbox.research.crossref.org")

        connector = claws.aws_utils.AWSConnector(
            unsigned=False, bucket=bucket, region_name="us-east-1"
        )

        return json.loads(connector.get_secret(secret_name))
