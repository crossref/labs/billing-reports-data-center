#!/bin/bash

# Ensure the script stops if any command fails
set -e

# Setup virtual environment
setup_venv() {
    echo "Checking for existing virtual environment..."
    if [ ! -d "./venv" ]; then
        echo "No virtual environment found. Creating one..."
        python3 -m venv venv
        echo "Virtual environment created."
    else
        echo "Virtual environment already exists."
    fi

    echo "Activating virtual environment..."
    source ./venv/bin/activate
    echo "Virtual environment activated."

    echo "Installing required packages..."
    pip install -r requirements.txt
    echo "Dependencies have been installed."
}

# Check if virtual environment is active
if [[ -z "$VIRTUAL_ENV" ]]; then
    setup_venv
else
    echo "Virtual environment is already active."
fi
