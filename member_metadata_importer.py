import json
import logging
from io import BytesIO
from zipfile import ZipFile

import requests

from crutils.admin_pages import (
    member_admin_page,
    member_admin_info,
    publisher_id,
    customer_id,
    description,
    member_admin_user_page,
    member_admin_user_info,
)
from crutils.common import user_selection, get_member_metadata_path

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)


class MemberMetadataImporter:
    MEMBERS_API_URL = "https://s3.us-east-1.amazonaws.com/outputs.research.crossref.org/api_artifacts/members.json"

    def __init__(self, logger):
        self.logger = logger

    def _read_members_file(self, members_data):
        logger.info("Reading members file")
        if isinstance(members_data, str):
            try:
                members = json.loads(members_data)
            except json.JSONDecodeError as e:
                logger.error(f"Failed to decode JSON: {str(e)}")
                return None
        elif isinstance(members_data, bytes):
            try:
                members = json.loads(members_data.decode('utf-8'))
            except (UnicodeDecodeError, json.JSONDecodeError) as e:
                logger.error(f"Failed to decode and parse JSON: {str(e)}")
                return None
        elif isinstance(members_data, dict) or isinstance(members_data, list):
            # Assume members_data is already a properly formatted JSON object
            members = members_data
        else:
            logger.error("Unsupported data type for members_data")
            return None

        logger.info("Finished reading members file")
        return members

    def _get_billing_metadata(self, member_record):
        member_id = member_record["id"]
        name = member_record["primary-name"]
        prefixes = member_record["prefixes"]
        billing_metadata = {"id": member_id, "name": name, "prefixes": {}}

        for prefix in prefixes:
            logger.info(f"prefix: {prefix}")
            admin_info = member_admin_info(member_admin_page(prefix))

            pub_id = publisher_id(admin_info)

            logger.info(f"publisher_id: {pub_id}")

            billing_metadata["prefixes"][prefix] = {
                "publisher-id": pub_id,
                "customer-id": customer_id(admin_info),
                "description": description(admin_info),
                "users": [
                    {
                        "user-id": user_info["user-id"],
                        "name": user_info["name"],
                    }
                    for user_info in member_admin_user_info(
                        member_admin_user_page(pub_id)
                    )
                ],
            }

        return billing_metadata

    def _sort_by_member_id(self, members):
        logger.info("Sorting members by member id")
        return sorted(members, key=lambda record: record["id"])
    
    def _get_member_data_file(self):
        try:
            response = requests.get(self.MEMBERS_API_URL)
            response.raise_for_status()  # Raises HTTP errors
            data = response.json()
            return data['message']['items']
        except requests.RequestException as e:
            self.logger.error("Failed to download member data file: %s", str(e))
        return None

    def _save_billing_metadata(self, billing_metadata, path):
        with open(path, "w") as billing_metadata_file:
            json.dump(billing_metadata, billing_metadata_file, indent=2)

    def import_member_metadata(self, results_dir):
        self.logger.send("Info", "Importing member metadata")

        member_data = self._get_member_data_file()
        if member_data is None:
            self.logger.error("Failed to retrieve member data")
            return

        members = self._sort_by_member_id(
            user_selection(
                self._read_members_file(member_data),
                member_id=None,
                max=None,
            )
        )

        logger.info(f"generating metadata for {len(members)} members")

        result: dict = {}

        for count, member in enumerate(members, start=1):
            logger.info(
                f"record {count} of {len(members)}: {member['primary-name']} ({member['id']})"
            )

            member_metadata_path = get_member_metadata_path(
                results_dir, member["id"]
            )
            logger.info(f"metadata file: {member_metadata_path}")
            if member_metadata_path.exists():
                logger.info(
                    f"Bulk mode: metadata file for {member['id']} already exists, skipping"
                )
                continue

            logger.info(
                f"generating metadata for member: {member['primary-name']} ({member['id']})"
            )

            self._save_billing_metadata(
                self._get_billing_metadata(member), member_metadata_path
            )

            logger.info(
                f"finished generating metadata for member: {member['primary-name']} ({member['id']})"
            )
            logger.info("-" * 80)

        self.logger.send("Info", "Member metadata imported")
        return
